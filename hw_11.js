let passwordIcons = document.querySelectorAll('.icon-password');

passwordIcons.forEach(icon =>{
    icon.addEventListener('click',()=>{
        const input = icon.previousElementSibling;
        const type = input.getAttribute('type') === 'password' ? 'text' : 'password';
        input.setAttribute('type', type);
    })
});

let form = document.querySelector('.password-form');
let btn = document.querySelector('.btn');

btn.addEventListener('click', event =>{
    event.preventDefault();

    let passwordInputs = form.querySelectorAll('input[type="password"]');

    let firstPassword = passwordInputs[0].value;
    let secondPassword = passwordInputs[1].value;

    if (firstPassword === secondPassword ){
        alert('you are welcome');
    } else {
        const errorText = document.createElement('span');
        errorText.textContent = 'Потрібно ввести однакові значення';
        errorText.style.color = 'red';
        passwordInputs[1].insertAdjacentElement('afterend', errorText);
    }
});
